package svm.instances.dependency;

import base.Kernel;
import base.OutputKernel;
import svm.SVMStructKernelInstance;
import svm.instances.dependency.kernel.DependencyTreeKernel;
import svm.instances.dependency.kernel.SequenceKernel;

public class DependencyParserKernelInstance implements SVMStructKernelInstance<TokenSequence, DependencyTree>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public OutputKernel<TokenSequence,DependencyTree> out = new DependencyTreeKernel();
	public Kernel<TokenSequence> in = new SequenceKernel();
	
	@Override
	public OutputKernel<TokenSequence,DependencyTree> outputKernel()
	{
		// TODO Auto-generated method stub
		return out;
	}

	@Override
	public Kernel<TokenSequence> inputKernel()
	{
		// TODO Auto-generated method stub
		return in;
	}

	@Override
	public double loss(DependencyTree y, DependencyTree predY)
	{
		assert predY != null;

		if (y.heads.length != predY.heads.length)
		{
			System.out.println("dafuq");
			return 1;
		}
		int errors = 0;
		for (int i = 0; i < y.heads.length; i++)
			if (y.heads[i] != predY.heads[i])
				errors++;
		return 1.0 * errors / y.heads.length;
	}
}
