package svm.instances.dependency;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import base.Example;

public class ConllLoader
{
	public static List<Example<TokenSequence, DependencyTree>> loadTreebank()
	{
		return loadTreebank(-1);
	}
	
	public static List<Example<TokenSequence, DependencyTree>> loadTreebank(String file,int max)
	{
		List<Example<TokenSequence, DependencyTree>> examples = new ArrayList<>();

		try
		{
			//BufferedReader in = new BufferedReader(new FileReader("data/german/tiger/train/german_tiger_train.conll"));
			BufferedReader in = new BufferedReader(new FileReader(file));

			ArrayList<String[]> buffer = new ArrayList<String[]>();
			for(String l = in.readLine();l!=null;l=in.readLine())
			{
				l = l.trim();
				if(l.equals(""))
				{
					TokenSequence x = new TokenSequence(buffer.size());
					DependencyTree y = new DependencyTree();
					y.x = x;
					y.heads = new int[buffer.size()];
					for(int i=0;i<buffer.size();i++)
					{
						x.tokens[i+1] = buffer.get(i)[1].toLowerCase().trim();
						x.tokens[i+1] = x.tokens[i+1].replaceAll("ö", "oe");
						x.tokens[i+1] = x.tokens[i+1].replaceAll("ä", "ae");
						x.tokens[i+1] = x.tokens[i+1].replaceAll("ü", "ue");
						x.tokens[i+1] = x.tokens[i+1].replaceAll("ß", "ss");
						//x.tokens[i+1] = x.tokens[i+1].replaceAll("[^a-z0-9.,?!]", "");
						x.lemmas[i+1] = buffer.get(i)[2].toLowerCase().trim();
						x.posTag[i+1] = buffer.get(i)[3].trim();
						x.posTag2[i+1] = buffer.get(i)[4].trim();
						y.heads[i] = Integer.parseInt(buffer.get(i)[6]);
					}
					Example<TokenSequence,DependencyTree> e = new Example<TokenSequence,DependencyTree>(x,y);
					e.i = examples.size();
					examples.add(e);
					buffer.clear();

					if(max>0 && examples.size()>=max)
					{
						in.close();
						return examples;
					}
				}
				else
				{
					String[] columns = l.split("\\s");
					buffer.add(columns);
				}
			}
			in.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();			
		}

		return examples;
	}
	
	public static List<Example<TokenSequence, DependencyTree>> loadTreebank(int max)
	{
		return loadTreebank("data/tuebadz.conll",max);
	}
	
	
}
