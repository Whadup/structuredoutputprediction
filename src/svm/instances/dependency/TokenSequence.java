package svm.instances.dependency;
import java.io.Serializable;
import java.util.Arrays;

import base.Structure;

public class TokenSequence implements Structure, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String[] tokens;
	public String[] lemmas;
	public String[] posTag;
	public String[] posTag2;
	public Integer hash;
	
	public TokenSequence(int n)
	{
		tokens = new String[n+1];
		tokens[0]="<root>";
		lemmas = new String[n+1];
		lemmas[0]="<root-lemma>";
		posTag = new String[n+1];
		posTag[0] = "<root-postag>";
		posTag2 = new String[n+1];
		posTag2[0] = "<root-postag2>";
	}

	@Override
	public String toString()
	{
		return "TokenSequence [tokens=" + Arrays.toString(tokens) + ", lemmas=" + Arrays.toString(lemmas) + ", posTag=" + Arrays.toString(posTag) + ", posTag2=" + Arrays.toString(posTag2) + "]";
	}

//	@Override
//	public int hashCode()
//	{
//		if(hash==null)
//		{
//			int result = 1;
//			String a = this.toString();
//			result = a.hashCode();
//			
//			hash=result;
//			return result;
//		}
//		else
//			return hash;
//		
//	}
//
//	@Override
//	public boolean equals(Object obj)
//	{
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		TokenSequence other = (TokenSequence) obj;
//		if (!Arrays.equals(lemmas, other.lemmas))
//			return false;
//		if (!Arrays.equals(posTag, other.posTag))
//			return false;
//		if (!Arrays.equals(posTag2, other.posTag2))
//			return false;
//		if (!Arrays.equals(tokens, other.tokens))
//			return false;
//		return true;
//	}
	
	
	
}
