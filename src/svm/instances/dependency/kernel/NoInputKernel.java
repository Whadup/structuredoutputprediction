package svm.instances.dependency.kernel;


import base.Kernel;
import svm.instances.dependency.TokenSequence;

public class NoInputKernel implements Kernel<TokenSequence>
{

	@Override
	public double k(TokenSequence a, TokenSequence b)
	{
		return 1;
	}

}
