package svm.instances.dependency;

import java.io.Serializable;
import java.util.Arrays;

import base.Structure;

public class DependencyTree implements Structure,Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public TokenSequence x;
	public int[] heads;
	@Override
	public String toString()
	{
		return "DependencyTree [x=" + x + ", heads=" + Arrays.toString(heads) + "]";
	}
	
	
	
	
}
