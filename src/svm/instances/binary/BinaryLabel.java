package svm.instances.binary;

import base.Structure;

public class BinaryLabel implements Structure
{
	double y;
	
	public static final BinaryLabel YPOS = new BinaryLabel(1);
	public static final BinaryLabel YNEG = new BinaryLabel(-1);
	
	public BinaryLabel(double y)
	{
		this.y = y >= 0 ? 1 : -1;
	}

	public double getY()
	{
		return y;
	}

	public void setY(double y)
	{
		this.y = y;
	}
	
	public String toString()
	{
		return ""+y;
	}

	public static BinaryLabel ypos()
	{
		// TODO Auto-generated method stub
		return new BinaryLabel(1);
	}

	public static BinaryLabel yneg()
	{
		// TODO Auto-generated method stub
		return new BinaryLabel(-1);
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BinaryLabel other = (BinaryLabel) obj;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}
	
	
}
