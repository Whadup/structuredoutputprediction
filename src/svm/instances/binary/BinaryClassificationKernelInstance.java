package svm.instances.binary;

import svm.SVMStructKernelInstance;
import base.DenseVector;
import base.DotKernel;
import base.Kernel;
import base.OutputKernel;

public class BinaryClassificationKernelInstance implements SVMStructKernelInstance<DenseVector, BinaryLabel>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public BinaryOutputKernel l = new BinaryOutputKernel();
	public DotKernel k = new DotKernel();
	
	@Override
	public OutputKernel<DenseVector,BinaryLabel> outputKernel()
	{
		// TODO Auto-generated method stub
		return l;
	}

	@Override
	public Kernel<DenseVector> inputKernel()
	{
		// TODO Auto-generated method stub
		return k;
	}

	@Override
	public double loss(BinaryLabel y, BinaryLabel ypred)
	{
		return (1-y.y*ypred.y)/2;
	}

}
