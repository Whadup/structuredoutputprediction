package svm.instances.precomputed;

import java.util.ArrayList;
import java.util.HashMap;

import base.SparseVector;
import base.Structure;

public class PrecomputedFeaturesInput extends ArrayList<SparseVector> implements Structure
{
	public ArrayList<Double> loss;
	public ArrayList<HashMap<String,String>> annotations;
	public PrecomputedFeaturesInput()
	{
		super();
		loss = new ArrayList<Double>();
		annotations = new ArrayList<HashMap<String,String>>();
	}
}
