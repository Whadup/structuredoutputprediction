package svm.instances.precomputed;

import base.Structure;

public class PrecomputedFeaturesOutput implements Structure
{
	public PrecomputedFeaturesOutput(PrecomputedFeaturesInput x, int maxi)
	{
		this.input = x;
		this.selected = maxi;
	}
	public int selected;
	public PrecomputedFeaturesInput input;
	@Override
	public String toString()
	{
		return ""+selected;
	}
	
	
}
