package svm;

import java.util.Set;

import base.SparseVector;
import base.Structure;

public interface SVMStructInstance<X extends Structure, Y extends Structure>
{
	public SparseVector phi(X x, Y y);
	public Y predict(X x, SVMStruct svm);
	public Y marginRescaling(X x, Y y, SVMStruct svm);
	public Y slackRescaling(X x, Y y, SVMStruct svm);
	public double loss(Y y, Y predY);
	public Set<String> features(X x,Y y);
}
