package svm;

import java.util.ArrayList;

import base.Structure;

public class DualConstraint<X extends Structure, Y extends Structure>
{
	public ArrayList<Y> ybar;
	public double loss;
	public ArrayList<Double> losses;
	public int lastUsed;
	
	
	public DualConstraint()
	{
		ybar = new ArrayList<>();
		losses = new ArrayList<Double>();
		loss=0;
	}
	
	public void addConstraint(X x, Y y, Y ybar, Mode m, double l)
	{
		this.ybar.add(ybar);
		this.losses.add(l);
		this.loss+=l;
	}
	
	public void finalizeConstraint(int n)
	{
		this.loss/=n;
	}
}
