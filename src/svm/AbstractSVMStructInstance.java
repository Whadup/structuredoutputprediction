package svm;

import java.util.Set;

import base.SparseVector;
import base.Structure;

public abstract class AbstractSVMStructInstance<X extends Structure,Y extends Structure> implements SVMStructInstance<X, Y>
{
	public Set<String> features(X x, Y y)
	{
		SparseVector phi = phi(x,y);
		return phi.keySet();
	}
}
