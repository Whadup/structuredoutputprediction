package svm;

import java.io.Serializable;

import base.Kernel;
import base.OutputKernel;
import base.Structure;

public interface SVMStructKernelInstance<X extends Structure, Y extends Structure> extends Serializable
{
	public OutputKernel<X,Y> outputKernel();
	public Kernel<X> inputKernel();
	public double loss(Y y, Y ypred);
}
