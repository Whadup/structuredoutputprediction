package base;

import svm.AtomicConstraint;

public interface AtomicKernel<X extends Structure,Y extends Structure> extends OutputKernel<X,Y>
{
	public SparseVector featureMap(X x, Y y);
	
	public Y preImage(AtomicConstraint<X,Y> w, X x, Kernel<X> inputKernel);
	public Y preImageL2Penelized(AtomicConstraint<X,Y> w, Y y, X x, Kernel<X> inputKernel);
}
