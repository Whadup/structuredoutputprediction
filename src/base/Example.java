package base;

import java.io.Serializable;


public class Example<X extends Structure, Y extends Structure> implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Example(X x, Y y)
	{
		this.x=x;
		this.y=y;
	}
	public X x;
	public Y y;
	public int i;
	
	@Override
	public String toString()
	{
		return "Example [x=" + x + ", y=" + y + "]";
	}
	
	
}
