package base;

import java.util.Map;


public interface OutputKernel<X extends Structure, Y extends Structure> extends Kernel<Y>
{
//	public X preImage(List<X> data, List<Double> alphas);
//	public X preImageL2Penelized(List<X> data, List<Double> alphas, X y);
	public Y preImage(Map<Y,Double> alphaY,X x);
	public Y preImagePenelized(Map<Y,Double> alphaY,Y y,X x);
}
