package base;

import svm.SVMStruct;

public class DenseVector implements Structure
{
	public DenseVector(int n)
	{
		x=new double[n];
	}
	
	public double[] x;
	
	public int length()
	{
		return x.length;
	}
	
	public double multiply(DenseVector e)
	{
		double s = 0;
		for(int i=0;i<x.length;i++)
			s+=x[i]*e.x[i];
		return s;
	}
	
	public void scale(double a)
	{
		for(int i=0;i<x.length;i++)
			x[i]*=a;
	}

	public int countNonZero()
	{
		int n = 0;
		for(int i=0;i<x.length;i++)
			if(Math.abs(x[i])>0.000001)
				n++;
		return n;
	}

	public void addScaled(DenseVector phi, double doubleValue)
	{
		for(int i=0;i<x.length;i++)
			x[i]+=doubleValue*phi.x[i];
		
	}
}
