package base;

public class DotKernel implements Kernel<DenseVector>
{
	@Override
	public double k(DenseVector a, DenseVector b)
	{
		double sum=0;
		for(int i=0;i<a.length();i++)
			sum+=a.x[i]*b.x[i];
		return sum;
	}

}
